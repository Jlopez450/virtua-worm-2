editor:
		move.w #$2700,sr
		move.b #$ff,userstage
		move.b #$ff,editorflag
		move.b #$01, selection
		bsr set_tile
		move.b #$00, mode
		lea (cursor),a5
		move.l #$68000003,(a3)
		move.w #$00c0,d4
		bsr vram_loop		
		move.l #$78000003,(a3)
		lea (cursordata),a5
		move.w #$0108,(a5)+ ;cursor y
		move.w #$0a01,(a5)+
		move.w #$8740,(a5)+ ;white
		move.w #$0158,(a5)+ ;cursor x	
		move.w #$0088,(a5)+ ;cursor y
		move.w #$0a00,(a5)+
		move.w #$a740,(a5)+ ;grey
		move.w #$0088,(a5)+ ;cursor x
		bsr update_cursor
		lea (background),a5
		lea (decloc),a0
		bsr decompress	
		lea (decloc),a5	
		move.l #$60000000,(a3)
		move.w #$2a3F,d4
		bsr vram_loop
		bsr map_background
		bsr ld_teststage		
		bsr draw_menu
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop
		move.l #$55cc0000,(a3) ;set visible deadzone
		move.w #$000F,(a4)
		move.w #$F000,(a4)
		move.w #$000F,(a4)
		move.w #$F000,(a4)
		;move.b #$01,musicnum
		;lea (ogmusic1)+66,a6
		;move.l a6,VGM_start
        move.w #$2300, sr       ;enable ints
		

editorloop:
		move.b #$ff,vb_flag
		bsr read_controller
		bsr test_input_editor
vb_wait_editor:
		tst.b vb_flag
		bne vb_wait_editor
		bra editorloop
		
vb_editor:
		movem.l d0/d1, -(sp)
		bsr music_driver ;Note: delays startup if no music loaded
		bsr update_hud
		bsr animate_tiles
		bsr update_cursor
		clr vb_flag	
		movem.l (sp)+, d0/d1
        rte
		
test_input_editor:
		tst.b inverter2
		beq return
		tst.b mode
		bne select
placement:
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq left_cur_place
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq right_cur_place
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe, d7
		beq up_cur_place
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd, d7
		beq down_cur_place
		move.b d3,d7
		or.b #$7f,d7
		cmpi.b #$7f, d7
		move.b d3,d7
		or.b #$7f,d7
		cmpi.b #$7f, d7
		beq setselect
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef, d7
		beq place_block	
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df, d7
		beq remove_block			
		rts
select:
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq left_cur	
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq right_cur
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe, d7
		beq up_cur	
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd, d7
		beq down_cur
		move.b d3,d7
		or.b #$7f,d7
		cmpi.b #$7f, d7
		beq setplace
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df, d7
		beq play_level
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef, d7
		beq savestage	
		rts
		
play_level:
		move.w #$2700,sr
		bsr count_food
		lea (ruletext),a5
		move.l #$0000a6b8,d5
		bsr draw_dialogue
ruleloop:
		bsr read_controller
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf, d7	;a
		beq legacy	
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef, d7	;b
		beq new	
		bra play_level
play_level_1:
		bsr set_start
		bsr savestage
		lea (cursordata),a5
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.b #$00,editorflag
		move.l #$55cc0000,(a3) ;set invisible deadzone
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		lea (clearruletext),a5
		move.l #$0000a6b8,d5
		bsr draw_dialogue
		bsr ld_userstage
		move.w #$2300,sr
		bra loop
		
set_start:
		lea (dirtext),a5
		move.l #$0000a6b8,d5
		bsr draw_dialogue
		lea (cursordata2),a5
		move.w #$0090,(a5)+ ;cursor y
		move.w #$0000,(a5)+
		move.w #$80fb,(a5)+ ;snake
		move.w #$0090,(a5)+ ;cursor x
		move.w #$0002,playerX; in tiles
		move.w #$0002,playerY; in tiles
		bsr update_cursor
set_startloop:
		move.w #$4fff,d4
set_wait:
		nop
		dbf d4, set_wait
		bsr update_cursor
		bsr read_controller
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq left_st
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq right_st
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe, d7
		beq up_st
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd, d7
		beq down_st
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df, d7
		beq return	
		bra set_startloop
		
left_st:
		lea (cursordata)+14,a5
		move.w (a5),d0
		cmpi.w #$0090, d0
		ble set_startloop
		sub.w #$0008,(a5) ;cursor y
		move.b #$03,direction
		sub.w #$01, playerx
		lea (cursordata2)+4,a5
		move.w #$80fe,(a5)+ ;snake	
		bra set_startloop
right_st:
		lea (cursordata)+14,a5
		move.w (a5),d0
		cmpi.w #$00148, d0
		bge set_startloop
		add.w #$0008,(a5) ;cursor y
		move.b #$04,direction
		add.w #$01, playerx
		lea (cursordata2)+4,a5
		move.w #$80fc,(a5)+ ;snake	
		bra set_startloop
up_st:
		lea (cursordata)+8,a5
		move.w (a5),d0
		cmpi.w #$0090, d0
		ble set_startloop
		sub.w #$0008,(a5) ;cursor y
		move.b #$01,direction
		sub.w #$01, playery
		lea (cursordata2)+4,a5
		move.w #$80fb,(a5)+ ;snake	
		bra set_startloop
down_st:
		lea (cursordata)+8,a5
		move.w (a5),d0
		cmpi.w #$00148, d0
		bge set_startloop
		add.w #$0008,(a5) ;cursor y
		move.b #$02,direction
		add.w #$01, playery
		lea (cursordata2)+4,a5
		move.w #$80fd,(a5)+ ;snake	
		bra set_startloop
		
legacy:
		move.b #$ff,rules
		bsr unhold2
		bra play_level_1
new:
		move.b #$00,rules
		move.w #$0999,totaldots
		bsr unhold2
		bra play_level_1
		
setplace:
		lea (cursordata)+4,a5
		move.w #$8740,(a5)+ ;gray
		lea (cursordata)+12,a5
		move.w #$a740,(a5)+ ;white
		eor.b #$ff,mode
		bra unhold
setselect
		lea (cursordata)+4,a5
		move.w #$a740,(a5)+ ;white
		lea (cursordata)+12,a5
		move.w #$8740,(a5)+ ;gray
		eor.b #$ff,mode
		bra unhold

left_cur_place:
		lea (cursordata)+14,a5
		move.w (a5),d0
		cmpi.w #$0080, d0
		ble return
		sub.w #$0008,(a5) ;cursor y
		bra skid
right_cur_place:
		lea (cursordata)+14,a5
		move.w (a5),d0
		cmpi.w #$00148, d0
		bge return
		add.w #$0008,(a5) ;cursor y
		bra skid
up_cur_place:
		lea (cursordata)+8,a5
		move.w (a5),d0
		cmpi.w #$0080, d0
		ble return
		sub.w #$0008,(a5) ;cursor y
		bra skid
down_cur_place:
		lea (cursordata)+8,a5
		move.w (a5),d0
		cmpi.w #$00148, d0
		bge return
		add.w #$0008,(a5) ;cursor y
		bra skid
		
left_cur:
		lea (cursordata)+6,a5
		move.w (a5),d0
		cmpi.w #$0158, d0
		ble return
		sub.w #$0010,(a5) ;cursor y
		sub.b #$01, selection
		bsr set_tile
		bra unhold
right_cur:
		lea (cursordata)+6,a5
		move.w (a5),d0
		cmpi.w #$01a8, d0
		bge return
		add.w #$0010,(a5) ;cursor y
		add.b #$01, selection
		bsr set_tile
		bra unhold
up_cur:
		cmpi.b #$06, selection
		ble return
		lea (cursordata),a5
		sub.w #$0010,(a5) ;cursor y
		sub.b #$06, selection
		bsr set_tile
		bra unhold
down_cur:
		cmpi.b #$13, selection
		bge return
		lea (cursordata),a5
		add.w #$0010,(a5) ;cursor y
		add.b #$06,selection
		bsr set_tile
		bra unhold
		
skid:	
		move.b d3,d7
		or.b #$af,d7
		cmpi.b #$af,d7
		beq place_block ;skid place if holding A+B
		move.b d3,d7
		or.b #$9f,d7
		cmpi.b #$9f,d7
		beq remove_block ;skid erase if holding A+C
		bra unhold
		
update_cursor:
		move.w #$07, d4
		lea (cursordata),a5
		move.l #$78000003,(a3)
		bra vram_loop
place_block:
		moveq.l #$00000000,d1
		moveq.l #$00000000,d0
		lea (cursordata2),a5;ypos
		move.w (a5),d1
		lea (cursordata2)+6,a5;xpos
		move.w (a5),d0	
		sub.w #$78,d1
		sub.w #$78,d0
		lsl.w #$03,d1
		lsr.w #$03,d0
		add.w d1,d0 		;all in d0
		add.l #level_buffer,d0  	
		move.l d0,a0
		move.b tile,(a0)
		moveq.l #$00000000,d1
		moveq.l #$00000000,d0
		lea (cursordata2),a5;ypos
		move.w (a5),d1
		lea (cursordata2)+6,a5;xpos
		move.w (a5),d0	
		sub.w #$78,d0
		lsr.w #$02,d0
		sub.w #$78,d1
		lsl.w #$04,d1
		add.w d1,d0 		;all in d0
		add.l #$0000a000,d0  
		bsr calc_vram	
		move.l d0,(a3)
		moveq.l #$00000000,d2
		move.b tile,d2
		add.w #$80,d2
		move.w d2,(a4)
		rts
		
remove_block:
		moveq.l #$00000000,d1
		moveq.l #$00000000,d0
		lea (cursordata2),a5;ypos
		move.w (a5),d1
		lea (cursordata2)+6,a5;xpos
		move.w (a5),d0	
		sub.w #$78,d1
		sub.w #$78,d0
		lsl.w #$03,d1
		lsr.w #$03,d0
		add.w d1,d0 		;all in d0
		add.l #level_buffer,d0  	
		move.l d0,a0
		move.b #$20,(a0)
		moveq.l #$00000000,d1
		moveq.l #$00000000,d0
		lea (cursordata2),a5;ypos
		move.w (a5),d1
		lea (cursordata2)+6,a5;xpos
		move.w (a5),d0	
		sub.w #$78,d0
		lsr.w #$02,d0
		sub.w #$78,d1
		lsl.w #$04,d1
		add.w d1,d0 		;all in d0
		add.l #$0000a000,d0  
		bsr calc_vram	
		move.l d0,(a3)
		move.w #$0020,(a4)
		rts
		
count_food:
		lea (level_buffer),a5
		move.w #$6ff,d4
		move.w #$0000,totaldots
		moveq.l #$00000000,d1
counterloop:
		move.b (a5)+,d2
		cmpi.b #"F",d2
		beq countfood
		cmpi.b #"f",d2
		beq countfood
		cmpi.b #"L",d2
		beq countfood
endloop:
		dbf d4, counterloop
		lea bcdout,a1
		move.l d1,d0
		bsr convert
		lea bcdout,a1
		bsr convert
		swap d1
		move.l d1,totaldots
		rts
	
countfood:
		add.w #$01,d1
		bra endloop
		
set_tile:
		move.b selection,d0
		cmpi.b #$1,d0
		beq set1
		cmpi.b #$2,d0
		beq set2
		cmpi.b #$3,d0
		beq set3
		cmpi.b #$4,d0
		beq set4
		cmpi.b #$5,d0
		beq set5
		cmpi.b #$6,d0
		beq set6
		cmpi.b #$7,d0
		beq set7
		cmpi.b #$8,d0
		beq set8
		cmpi.b #$9,d0
		beq set9
		cmpi.b #$a,d0
		beq seta
		cmpi.b #$b,d0
		beq setb
		cmpi.b #$c,d0
		beq setc
		cmpi.b #$d,d0
		beq setd
		cmpi.b #$e,d0
		beq sete
		cmpi.b #$f,d0
		beq setf
		cmpi.b #$10,d0
		beq set10
		cmpi.b #$11,d0
		beq set11
		cmpi.b #$12,d0
		beq set12
		
		cmpi.b #$13,d0;13-17 music
		beq set13
		cmpi.b #$14,d0
		beq set14
		cmpi.b #$15,d0
		beq set15
		cmpi.b #$16,d0
		beq set16
		cmpi.b #$17,d0
		beq set17
		cmpi.b #$18,d0;extra life
		beq set18
		rts
		
set1:
		move.b #"W",tile
		rts
set2:
		move.b #"w",tile
		rts
set3:
		move.b #"a",tile
		rts
set4:
		move.b #"b",tile
		rts
set5:
		move.b #"c",tile
		rts
set6:
		move.b #"F",tile
		rts		
set7:
		move.b #"A",tile
		rts
set8:
		move.b #"B",tile
		rts
set9:
		move.b #"C",tile
		rts		
seta:
		move.b #"D",tile
		rts
setb:
		move.b #"d",tile
		rts
setc:
		move.b #"f",tile
		rts	
setd:
		move.b #"X",tile
		rts
sete:
		move.b #"T",tile
		rts
setf:
		move.b #"t",tile
		rts		
set10:
		move.b #"I",tile
		rts
set11:
		move.b #"i",tile
		rts
set12:
		move.b #".",tile
		rts
set13:
		move.b #$01,musicnum
		lea (ogmusic1)+66,a6
		move.l a6,VGM_start
		lea (music_null),a6
		rts		
set14:
		move.b #$02,musicnum
		lea (ogmusic2)+66,a6
		move.l a6,VGM_start
		lea (music_null),a6
		rts		
set15:
		move.b #$03,musicnum
		lea (ogmusic3)+66,a6
		move.l a6,VGM_start
		lea (music_null),a6
		rts		
set16:
		move.b #$04,musicnum
		lea (music1)+66,a6
		move.l a6,VGM_start
		lea (music_null),a6
		rts		
set17:
		move.b #$05,musicnum
		lea (music2)+66,a6
		move.l a6,VGM_start
		lea (music_null),a6
		rts		
set18:
		move.b #"L",tile
		rts				
		
		
draw_menu:
		lea (ed0),a5
		move.l #$69360002,(a3)
		move.w #$000b,d4
l0:
		move.b (a5)+,d5	
		add.w #$0080,d5
        move.w d5,(a4)
		dbf d4, l0
		lea (ed1),a5
		move.l #$6a360002,(a3)
		move.w #$000b,d4
l1:
		move.b (a5)+,d5	
		add.w #$0080,d5
        move.w d5,(a4)
		dbf d4, l1
		lea (ed2),a5
		move.l #$6b360002,(a3)
		move.w #$000a,d4
l2:
		move.b (a5)+,d5	
		add.w #$0080,d5
        move.w d5,(a4)
		dbf d4, l2
        move.w #"X",(a4)
		lea (ed3),a5
		move.l #$6c360002,(a3)
		move.w #$000b,d4
l3:
		move.b (a5)+,d5	
		add.w #$0080,d5
        move.w d5,(a4)
		dbf d4, l3		
		rts
		
unhold:
		bsr read_controller
		cmpi.w #$fff,d3
		 beq return
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf, d7	;a
		 beq return
		bra unhold
		
unhold2:
		bsr read_controller
		cmpi.w #$fff,d3
		 beq return
		bra unhold2
		
load_music:
		cmpi.b #$01,musicnum
		beq set13
		cmpi.b #$02,musicnum
		beq set14
		cmpi.b #$03,musicnum
		beq set15
		cmpi.b #$04,musicnum
		beq set16
		cmpi.b #$05,musicnum
		beq set17
		rts
		
savestage: ;Note to self: Finally works on real hardware, don't fuck with it anymore.
		lea ($200001),a5
		lea (level_buffer),a6
		move.w #$6FF,d4
		move.b musicnum, (a5)
		addq #2, a5
		move.b rules, (a5)
		addq #2, a5 
		move.w totaldots,d0
		move.b d0, (a5)
		addq #2, a5
		lsr.w #$08,d0
		move.b d0, (a5)
		addq #2, a5 
		move.b direction, (a5)
		addq #2, a5 		
saveloop:
		move.b (a6)+,(a5)
		addq #2, a5 
		dbf d4,saveloop			
		rts
		
load_to_editor:
		lea ($200001),a5
		lea (level_buffer),a6
		move.l #$60000002,(a3)
		move.l #$00000000,d0
		move.w #$6FF,d4	
		move.b (a5),musicnum
		addq #2, a5 
		move.b (a5),rules
		addq #2, a5 
		clr d1
		clr d3
		move.b (a5),d1
		addq #2, a5 
		move.b (a5),d3
		addq #2, a5 
		lsl.w #$8,d3
		eor.w d3,d1
		move.w d0,totaldots
		addq #2, a5 
		move.b (a5),direction

sram_load_loop:
		move.b (a5),d0
		move.b d0,(a6)+
		add.w #$80,d0
		move.w d0,(a4)
		addq #2, a5 
		dbf d4,sram_load_loop	
		rts
		
