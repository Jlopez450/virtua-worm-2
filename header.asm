 			  dc.l $00000000, start, BusError, AddressError
		   	  dc.l IllegalInstr, ZeroDivide, ChkInstr, TrapvInstr
		   	  dc.l privilegevio, Trace, Line1010Emu, Line1111Emu
			  dc.l ErrorTrap
	          dc.b "DON'T LOOK AT THIS!", $00
	          dc.l ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l HBlank,    ErrorTrap, VBlank,    ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.b "SEGA MEAGSIS    "
              dc.b "(C) 2018.OCT JAL"
              dc.b "Virtua Worm 2                                   "
              dc.b "Virtua Worm 2                                   "
              dc.b "GM 01234567-89"
              dc.w $DEAD        ;checksum
              dc.b "J               "
              dc.l 0
              dc.l ROM_End
              dc.l $FF0000
              dc.l $FFFFFF
              dc.b "RA", $F8, $20
sram_start:   dc.l $200000
sram_end:     dc.l $210000
              dc.b "            "                          
              dc.b "This program contains blast processing! "
              dc.b "JUE             "