    include "ramdat.asm"
    include "header.asm"
start:                         
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
		move.l #$00000000,a7
 		move.w #$100,($A11100)
		move.w #$100,($A11200)  
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram
		bsr region_check
		move.w #$03,lives
		move.l #$40000010,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		
		move.b #$01,stage
		move.l #$40000000,(a3)
		lea (font),a5
		move.w #$FFF,d4
		bsr vram_loop
		bsr intro		
		bsr title
		; bra editor
		lea (background),a5
		lea (decloc),a0
		bsr decompress	
		lea (decloc),a5	
		move.l #$60000000,(a3)
		move.w #$2a3F,d4
		bsr vram_loop
		bsr map_background
		bsr newstage
        move.w #$2300, sr       ;enable ints		
		
loop:
		tst.b splashflag
		bne loop
		move.b #$ff,vb_flag
		bsr read_controller
		bsr test_input
		bsr move_player		
		bsr randomnumx
		bsr draw_head
		bsr animate_tiles
vb_wait:
		bsr randomnumy
		tst.b vb_flag
		bne vb_wait
		bra loop
		
animate_tiles:
		eor.b #$ff,inverter2
		tst.b inverter2
		beq return
		add.b #$01,f_frame
		cmpi.b #$04,f_frame
		bne anim_cont
		move.b #$00,f_frame
anim_cont:		
		cmpi.b #$00,f_frame
		beq frame0
		cmpi.b #$01,f_frame
		beq frame1
		cmpi.b #$02,f_frame
		beq frame2
		cmpi.b #$03,f_frame
		beq frame3
		rts
		
frame0:
		lea (f_frame0),a5
		move.l #$5cc00000,(a3)	;vram 1cc0
		move.w #$000e,d4
		bsr vram_loop
		rts
frame1:
		lea (f_frame1),a5
		move.l #$5cc00000,(a3)	;vram 1cc0
		move.w #$000e,d4
		bsr vram_loop
		rts
frame2:
		lea (f_frame2),a5
		move.l #$5cc00000,(a3)	;vram 1cc0
		move.w #$000e,d4
		bsr vram_loop
		rts
frame3:
		lea (f_frame3),a5
		move.l #$5cc00000,(a3)	;vram 1cc0
		move.w #$000e,d4
		bsr vram_loop
		rts
		
randomnumy:
		add.w #$01, randomy
		cmpi.w #$1a,randomy
		blt return
		move.w #$02, randomy
		rts
randomnumx:
		add.w #$02, randomx
		cmpi.w #$36,randomx ;32 for "safe"
		blt return
		move.w #$04, randomx
		rts

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)
        dbf d4,vram_Loop
        rts  
clear_regs:
		moveq #$00000000,d0
		move.l d0,d1
		move.l d0,d2
		move.l d0,d3
		move.l d0,d4
		move.l d0,d5
		move.l d0,d6
		move.l d0,d7
		move.l d0,a0
		move.l d0,a1
		move.l d0,a2
		move.l d0,a3
		move.l d0,a4
		move.l d0,a5
		move.l d0,a6
		rts
		
clear_ram:
		move.w #$7FF8,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts		
smalltextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
textloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return			 
		andi.w #$00ff,d5
		;or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		bra smalltextloop
		
build_hud:
		move.l #$0000a036,d3
hudloop:
		move.w #$000c,d4		;draw 12 cells of text
		clr d5
		move.l d3,d0
		bsr calc_vram
		move.l d0,(a3)
textloop2:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return		 
		andi.w #$00ff,d5
		;or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop2	
		add.l #$0080,d3
		bra hudloop
		
; build_stage: ;old 64 column version
		; lea (level_buffer),a6
 		; move.b #$9F,$c00011
		; move.b #$EF,$c00011	;kill psg
        ; move.b #$FF,$c00011		
        ; move.b #$BF,$c00011	
		; clr d7
; build_stageloop:
		; clr d5
; textloop3:
		; move.b (a5)+,d5	
		; cmpi.b #$25,d5			;% (end of text flag)
		 ; beq return
		; andi.w #$00ff,d5
        ; move.b d5,(a6)+			;collision
		; add.w #$0080,d5			;get correct tile
        ; move.w d5,(a4)	
		; move.w #$0800,d4
		; bra build_stageloop
		
build_stage: ;new 40 line version
		lea (level_buffer),a6
 		move.b #$9F,$c00011
		move.b #$EF,$c00011	;kill psg
        move.b #$FF,$c00011		
        move.b #$BF,$c00011	
		clr d7
build_stageloop:
		clr d5
		move.w #39,d4
textloop3:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return
		andi.w #$00ff,d5
        move.b d5,(a6)+			;collision
		add.w #$0080,d5			;get correct tile
        move.w d5,(a4)
		dbf d4, textloop3
		move.w #23,d4
		;bra return
nullloop:
		move.w #$0000,(a4)
		move.b #$00,(a6)+
		dbf d4,nullloop
		bra build_stageloop
		
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
calc_vram_read:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$0000,d0  ;attach vram read bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
		
; read_controller:                ;d3 will have final controller reading!
		; moveq	#0, d3            
	    ; moveq	#0, d7
	    ; move.b  #$40, ($A10009) ;Set direction
	    ; move.b  #$40, ($A10003) ;TH = 1
    	; nop
	    ; nop
	    ; move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    ; andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    ; move.b	#$0, ($A10003)  ;TH = 0
	    ; nop
	    ; nop
	    ; move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    ; andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    ; lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    ; or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		; rts	
		
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3
		moveq	#0, d7
		move.b  #$40, ($A10009)	; Set direction
		move.b  #$40, ($A10003)	; TH = 1
		nop
		nop
		move.b  ($A10003), d3	; d3.b = X ; 1 ; C ; B ; R ; L ; D ; U ;
		andi.b	#$3F, d3		; d3.b = 0 ; 0 ; C ; B ; R ; L ; D ; U ;
		move.b	#$0, ($A10003)	; TH = 0
		nop
		nop
		move.b	($A10003), d7	; d7.b = X ; 0 ; S ; A ; 0 ; 0 ; D ; U ;
		andi.b	#$30, d7		; d7.b = 0 ; 0 ; S ; A ; 0 ; 0 ; 0 ; 0 ;
		lsl.b	#$2, d7			; d7.b = S ; A ; 0 ; 0 ; D ; U ; 0 ; 0 ;
		or.b	d7, d3			; d3.b = S ; A ; C ; B ; R ; L ; D ; U ;
		move.b  #$40, ($A10003)	; TH = 1
		nop
		nop
		move.b	#$0, ($A10003)	; TH = 0
		nop
		nop
		move.b	#$40, ($A10003)	; TH = 1
		nop
		nop
		move.b	($A10003), d7	; d7.b = X ; X ; X ; X ; M ; X ; Y ; Z
		move.b  #$0, ($A10003)	; TH = 0
		andi.w	#$F, d7			; d7.b = 0 ; 0 ; 0 ; 0 ; M ; X ; Y ; Z
		lsl.w	#$8, d7			; d7.w = 0 ; 0 ; 0 ; 0 ; M ; X ; Y ; Z ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ;
		or.w	d7, d3			; d3.w = 0 ; 0 ; 0 ; 0 ; M ; X ; Y ; Z ; S ; A ; C ; B ; R ; L ; D ; U ;
		rts			
		
return:
		rts
returnint:
		rte
HBlank:
		add.w #$01,hblanks
		move.l #$40000010,(a3) ;write to VSRAM
		move.w hblanks,d0    
		mulu.w d7,d0	   		   
		lsr #$06,d0	   
		sub.w d7, d0
		sub.w d7, d0
		move.w d0,(a4)   
		move.w d0,(a4)
		rte	

VBlank:
		tst.b splashflag
		bne calc_script	
		tst titleflag
		bne VB_title
		tst editorflag
		bne vb_editor
		movem.l d0/d1, -(sp)
		bsr music_driver
		bsr music_driver_frenzy
		bsr clock
		bsr update_hud
		clr vb_flag	
		movem.l (sp)+, d0/d1
		move.b #$df,$C00011 ;psg off
        rte
update_hud:
		move.l #$62c00002,(a3)
		moveq #$00000000,d0
		move.w dots,d0
		andi.w #$0F00,d0
		lsr.w #$08,d0
		add.w #$0030,d0
		move.w d0,(a4)
		move.w dots,d0
		andi.w #$00F0,d0
		lsr.w #$04,d0
		add.w #$0030,d0
		move.w d0,(a4)
		move.w dots,d0
		andi.w #$000F,d0
		add.w #$0030,d0
		move.w d0,(a4)
		
		move.l #$61c80002,(a3)		
		move.b seconds, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0		
		move.w d0, (a4)
		move.b seconds, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0		
		move.w d0, (a4)	
		
		move.l #$61c20002,(a3)	
		move.b minutes, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0	
		move.w d0, (a4)
		move.b minutes, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)	

		move.l #$63c40002,(a3)	
		move.b stage, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0	
		move.w d0, (a4)
		move.b stage, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)

		move.l #$62c80002,(a3)
		move.w totaldots,d0
		andi.w #$0F00,d0
		lsr.w #$08,d0
		add.w #$0030,d0
		move.w d0,(a4)
		move.w totaldots,d0
		andi.w #$00F0,d0
		lsr.w #$04,d0
		add.w #$0030,d0
		move.w d0,(a4)
		move.w totaldots,d0
		andi.w #$000F,d0
		add.w #$0030,d0
		move.w d0,(a4)	

		move.l #$64c60002,(a3)
		move.b frenzycounter, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0	
		move.w d0, (a4)
		move.b frenzycounter, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)
		
		move.l #$65c40002,(a3)
		move.w lives, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0	
		move.w d0, (a4)
		move.w lives, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)		
		rts
		
		
draw_head:		
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1	
		lsl.w #$03,d0
		lsl.w #$03,d1	
		add.w #$0080,d1
		add.w #$0080,d0
		
		move.l #$78000003,(a3)			
		cmpi.b #$01,direction
		beq headup
		cmpi.b #$02,direction
		beq headdown
		cmpi.b #$03,direction
		beq headleft
		cmpi.b #$04,direction
		beq headright
		rts
		
headright:
		move.w d1,(a4)
		move.w #$0000,(a4)
		move.w #$00fc,(a4)
		move.w d0,(a4)
		rts
headleft:	
		move.w d1,(a4)
		move.w #$0000,(a4)
		move.w #$00fE,(a4)
		move.w d0,(a4)
		rts
headdown:
		move.w d1,(a4)
		move.w #$0000,(a4)
		move.w #$00fd,(a4)
		move.w d0,(a4)
		rts
headup:	
		move.w d1,(a4)
		move.w #$0000,(a4)
		move.w #$00fb,(a4)
		move.w d0,(a4)
		rts
			
move_player:		
		tst.b frenzyflag
		bne move_player_frenzy
		add.b #$01,counter1
		move.b speed,d2
		cmp.b counter1,d2
		bne return
		clr turn
		clr counter1
		cmpi.b #$01,direction
		beq moveup
		cmpi.b #$02,direction
		beq movedown
		cmpi.b #$03,direction
		beq moveleft
		cmpi.b #$04,direction
		beq moveright
		rts
move_player_frenzy:		
		add.b #$01,counter1
		move.b speed_frenzy,d2
		cmp.b counter1,d2
		bne return
		clr turn
		clr counter1
		cmpi.b #$01,direction
		beq moveup
		cmpi.b #$02,direction
		beq movedown
		cmpi.b #$03,direction
		beq moveleft
		cmpi.b #$04,direction
		beq moveright
		rts
;Note: Collision detection scheme changed from first game.
;Each wall type type is no longer individually checked.
;Instead, movement is halted if niether food or empty space is found.
moveup:
		move.w #$FFC0,d2
		bsr collision

		cmpi.b #$0046,d5 ;food
		beq up_food
		cmpi.b #$0066,d5 ;frenzy
		beq up_frenzy
		cmpi.b #$004c,d5 ;life
		beq up_life	
		cmpi.b #$20,d5 ;space tile
		bne return
		
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playery
		bsr check_selfhit		
		bsr remove_segment2
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		rts	
up_life:
		move.b #$20,(a0)	;clear object from level buffer
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playery	
		bsr render_segment
		move.w #$00FF,d2
		bsr render_pleyer
		bsr add_dot
		move.w lives,d1
		move.w #$01,d0
		abcd d0,d1
		move.w d1,lives			
		rts

up_frenzy:	
		move.b #$20,(a0)	;clear object from level buffer
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playery	
		bsr render_segment

		move.w #$00FF,d2
		bsr render_pleyer
		bsr add_dot
		bsr frenzymode
		rts
		
up_food:	
		move.b #$20,(a0)	;clear object from level buffer
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playery	
		bsr render_segment
		move.w #$00FF,d2
		bsr render_pleyer
		bsr add_dot
		rts
movedown:
		move.w #$0040,d2
		bsr collision
		cmpi.b #$0046,d5 ;food
		beq down_food
		cmpi.b #$0066,d5 ;frenzy
		beq down_frenzy
		cmpi.b #$004c,d5 ;life
		beq down_life
		cmpi.b #$20,d5 ;space tile
		bne return
		
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playery
		bsr check_selfhit		
		bsr remove_segment2
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		rts
down_life:		
		move.b #$20,(a0)	;clear object from level buffer
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playery	
		bsr render_segment
		move.w #$00FF,d2
		bsr render_pleyer
		bsr add_dot
		move.w lives,d1
		move.w #$01,d0
		abcd d0,d1
		move.w d1,lives			
		rts
		
down_frenzy:	
		move.b #$20,(a0)
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playery	
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		bsr add_dot
		bsr frenzymode
		rts
down_food:	
		move.b #$20,(a0)
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playery	
		;bsr remove_segment2
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		bsr add_dot
		rts
		
moveleft:
		move.w #$FFFF,d2
		bsr collision
		cmpi.b #$0046,d5 ;food
		beq left_food
		cmpi.b #$0066,d5
		beq left_frenzy
		cmpi.b #$004c,d5 ;life
		beq left_life
		cmpi.b #$20,d5 ;space tile
		bne return
		
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playerX
		bsr check_selfhit		
		bsr remove_segment2
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		rts

left_life:	
		move.b #$20,(a0)
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playerX	
		bsr render_segment
		move.w #$00Ff,d2
		bsr render_pleyer
		bsr add_dot
		move.w lives,d1
		move.w #$01,d0
		abcd d0,d1
		move.w d1,lives			
		rts
		
left_frenzy:	
		move.b #$20,(a0)
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playerX	
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		bsr add_dot
		bsr frenzymode
		rts
		
left_food:	
		move.b #$20,(a0)
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		sub.w #$0001,playerX	
		bsr render_segment

		move.w #$00Ff,d2
		bsr render_pleyer
		bsr add_dot
		rts
		
moveright:	
		moveq #$00000001,d2
		bsr collision	
		cmpi.b #$0046,d5 ;food
		beq right_food
		cmpi.b #$0066,d5 ;frenzy
		beq right_frenzy
		cmpi.b #$004c,d5 ;life
		beq right_life
		cmpi.b #$20,d5 ;space tile
		bne return
		
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playerX	
		bsr check_selfhit
		bsr remove_segment2
		bsr render_segment
		move.w #$00FF,d2
		bsr render_pleyer
		rts
		
right_life:	
		move.b #$20,(a0)
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playerX	
		bsr render_segment
		move.w #$00Ff,d2
		bsr render_pleyer
		bsr add_dot
		move.w lives,d1
		move.w #$01,d0
		abcd d0,d1
		move.w d1,lives			
		rts

right_frenzy:	
		move.b #$20,(a0)
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playerX	
		;bsr remove_segment2
		bsr render_segment

		move.w #$00FF,d2
		bsr render_pleyer
		bsr add_dot
		bsr frenzymode
		rts
		
right_food:	
		move.b #$20,(a0)
		move.w playery,(a1)+		
		move.w playerx,(a1)+
		add.w #$0001,playerX	
		;bsr remove_segment2
		bsr render_segment

		move.w #$00FF,d2
		bsr render_pleyer
		bsr add_dot
		rts

		
		
collision:
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1	
		lsl.w #$06,d1
		add.w d2,d0
		add.w d1,d0 		;all in d0
		add.l #level_buffer,d0  	
		move.l d0,a0
		move.b (a0),d5
		rts
test_placement:
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w randomx,d0
		move.w randomy,d1	
		lsl.w #$06,d1
		add.w d1,d0 		;all in d0
		add.l #level_buffer,d0  	
		move.l d0,a0
		move.b (a0),d5
		rts
		
check_selfhit:		
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1	
		lsl.w #$01,d0
		lsl.w #$07,d1
		add.l #$0080,d0
		sub.l #$0080,d1
		add.w d1,d0 		;all in d0
		add.l #$0000a000,d0  
		bsr calc_vram_read		
		move.l d0,(a3)
		move.w (a4),d0
		cmpi.w #$00FF,d0
		beq kill
		rts
		
kill:
		move.w #$2700,sr
		move.b #$00,frenzycounter
		move.b #$00,frenzyflag
 		move.b #$9F,$c00011
		move.b #$EF,$c00011	;kill psg
        move.b #$FF,$c00011		
        move.b #$BF,$c00011	
		cmpi.w #$0000,lives
		beq gameover
		
		move.w lives,d1
		move.w #$01,d0
		sbcd d0,d1
		move.w d1,lives		
		;sub.b #$01,lives
		lea (killtext),a5
		move.l #$0000a602,d5
		bsr draw_dialogue
killloop:
		bsr read_controller
		cmpi.b #$7f,d3
		beq retry	;stack growth?
		bra killloop
retry:	
		tst.b userstage
		bne load_editor_sram
		bsr newstage
		move.w #$2300,sr ;some fixing due to moving this code off of vblank
		add.l #$08,a7
		bra loop
		;rte
		
gameover:
		bra no_tmss
		
render_pleyer:
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1	
		lsl.w #$01,d0
		lsl.w #$07,d1
		add.l #$0080,d0
		sub.l #$0080,d1
		add.w d1,d0 		;all in d0
		add.l #$0000a000,d0  	
		bsr calc_vram
		move.l d0,(a3)
		move.w d2,(a4)
		rts
		
render_segment:
		moveq #$00000000,d0
		moveq #$00000000,d1
		move.w playerx,d0
		move.w playery,d1			
		lsl.w #$01,d0
		lsl.w #$07,d1		
		add.w #$0080,d0
		sub.w #$0080,d1
		add.w d1,d0 		;all in d0		
		add.l #$0000a000,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$00FF,(a4)
		rts
		
remove_segment:
		moveq #$00000000,d0
		moveq #$00000000,d1	
		move.w -(a1),d0	;x
		move.w -(a1),d1	;y	
		lsl.w #$01,d0
		lsl.w #$07,d1		
		add.w #$0080,d0
		sub.w #$0080,d1
		add.w d1,d0 		;all in d0		
		add.l #$0000a000,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)
		rts
remove_segment2:
		moveq #$00000000,d0
		moveq #$00000000,d1	
		move.w (a2)+,d1	;y			
		move.w (a2)+,d0	;x
		lsl.w #$01,d0
		lsl.w #$07,d1		
		add.w #$0080,d0
		sub.w #$0080,d1
		add.w d1,d0 		;all in d0		
		add.l #$0000a000,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)
		rts
		
add_dot:
        move.b #$af,$C00011 ;psg chirp
        move.b #$d0,$C00011 
        move.b #$cf,$C00011 
        move.b #$50,$C00011 ;tone
		; move.b #$9f,$C00011 
		; move.b #$d0,$C00011 
		; move.b #$cf,$C00011 
		; move.b #$50,$C00011 
		bsr add_food
		move.w #$01,d0
		move.w dots,d1
		cmpi.b #$99,d1
		beq centi
		abcd d0,d1
		move.w d1,dots
cenret:
		move.w totaldots,d0
		cmp.w d0,d1
		beq levelup	
		rts

		
add_food:
		tst.b rules ;if old style rules
		bne return
		bra check_placement
success:
		move.l #$00000000,d1	
		move.l d1,d0	
		move.w randomx,d1;x dot pos
		move.w randomy,d0;y dot pos
		
		lsl.w #$07,d0
		add.w #$a000,d0
		add.w d1,d0
		bsr calc_vram
		
		move.l d0,(a3)
		move.w #$00c6,(a4)
		
		move.l #$00000000,d1	
		move.l d1,d0		
		move.w randomx,d1;x dot pos
		move.w randomy,d0;y dot pos
		lsr.w #$01,d1
		lsl.w #$06,d0
		add.l #$FF00F0, d0
		add.w d1,d0
		move.l d0,a5
		move.b #$46, (a5)
		rts
check_placement: ;randomness is not so random. Need fixing
		move.l #$00000000,d1	
		move.l d1,d0
		move.w randomx,d1;x dot pos
		move.w randomy,d0;y dot pos
		
		lsl.w #$07,d0
		add.w #$a000,d0
		add.w d1,d0
		bsr calc_vram_read
		move.l d0,(a3)
		move.w (a4),d5
		cmpi.w #$0000,d5
		beq success
		cmpi.w #$00A0,d5
		beq success
		bsr randomnumx
		bsr randomnumy		
		bra check_placement
		
centi:
		add.w #$0067,d1
		move.w d1,dots
		bra cenret
		
hang:
		move.w #$2700,sr
		bra hang
levelup:
		bsr draw_head	;fix for misplaced head after win
		move.w #$0001,d0
		move.b stage,d1
		abcd d1,d0
		move.b d0,stage
		bra newstage
		
test_input:
		; move.b d3,d7
		; or.b #$bf,d7
		; cmpi.b #$bf, d7	;a
		; beq add_food
		tst turn
		bne return
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq left	
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq right					
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe, d7
		beq up	
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd, d7
		beq down
		; move.b d3,d7
		; or.b #$ef,d7
		; cmpi.b #$ef, d7	;b
		; beq levelup	
		move.b d3,d7
		or.b #$7f,d7
		cmpi.b #$7f, d7	;start
		beq quit			
		rts
		
quit:
		tst.b userstage
		beq return
		bra load_editor_sram
		
frenzymode:
		move.b #$ff,frenzyflag
		move.b frenzycounter,d0
		move.b #$05,d1
		abcd d1,d0
		move.b d0,frenzycounter
		;move.b #$05,frenzycounter
		lea (palette2),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop
		rts

		
up:
		cmpi.b #$02,direction
		beq return
		clr counter1	;greatly improves quick turns
		move.b #$01,direction
		move.b #$ff,turn
		rts
down:
		cmpi.b #$01,direction
		beq return
		clr counter1
		move.b #$02,direction
		move.b #$ff,turn
		rts
left:
		cmpi.b #$04,direction
		beq return
		clr counter1
		move.b #$03,direction
		move.b #$ff,turn
		rts
right:
		cmpi.b #$03,direction
		beq return
		clr counter1
		move.b #$04,direction
		move.b #$ff,turn
		rts
		
map_background:
        move.l #$4100,d0         ;first tile
        move.w #$001c,d5
        move.l #$40820003,(a3) ;vram write to $c082	   
superloop2:
        move.w #$19,d4
maploop3:
        move.w d0,(a4)
        add.w #$1,d0
        dbf d4,maploop3
        move.w #$25,d4
maploop4:
        move.w #$00,(a4)
        dbf d4,maploop4
        dbf d5,superloop2
        rts
		
		
intro:
		lea (splashscreen),a5
		lea (decompression_buffer), a0
        bsr decompress			
        move.l  #$70000000,(a3)  ;set VRAM write    
        move.w  #$4600, d4
		lea (decompression_buffer),a5
		bsr vram_loop		
		move.l #$40000003,(a3)	;vram write $c000	
		bsr generate_map_splash
		
		lea (font),a5
		move.l #$40000000,(a3)
		move.w #$07FF,d4
		bsr vram_loop	
		
		lea (palette_splash),a5
		move.w #$002f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		
		move.w #$8230,(a3)
		move.w #$8A00,(a3)
		move.w #$FFBE, d7		
		move.b #$ff,splashflag		
        move.w #$2300, sr       ;enable ints		
splashwait:
		nop
		tst splashflag
		bne splashwait
		move.w #$2700, sr	
		lea (splashtext),a5
		move.l #$0000cd16,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr overlay_text
		move.w #$0001,d4	
	    move.b #$2b,$A04000         ;DAC enable register             
	    move.b #$80,$A04001         ;enable DAC	
	    move.b #$2a,$A04000	
		lea (sfx_slogan),a5
		move.w #$FFFF,d4		 ;comment out to skip pause
splashhold:
		bsr test2612
		move.b (a5)+,$A04001
		move.w #$000C,d0
		bsr delay22khz
		dbf d4,splashhold
		rts
delay22khz:
		nop
		dbf d0,delay22khz
		rts
		
overlay_text:			;for strings with terminators
		move.b (a5)+,d4	
		cmpi.b #$24,d4	;$ (end of text flag)
		beq return
		andi.w #$00ff,d4
		eor.w #$2000,d4
        move.w d4,(a4)		
		bra overlay_text

generate_map_splash:                  ;generate a map for splash
        move.w #$001c,d5
        move.l #$00008180,d0         ;first tile		
superloop_splash:
        move.w #$27,d4
maploop_splash:
        move.w d0,(a4)
        add.w #$1,d0
        dbf d4,maploop_splash
        move.w #$17,d4
maploop2_splash:
        move.w #$00,(a4)
        dbf d4,maploop2_splash
        dbf d5,superloop_splash
        rts

calc_script:
	    move.w #$0000,hblanks 
	 	add.w #$0001,d7
		cmpi.w #$0000,d7
		beq end_splash
		rte
end_splash:
		move.w #$8AFF,(a3)	
        move.l #$40000010,(a3)   ;write to vsram   
		move.w #$0000,(a4)
		move.w #$0000,(a4)	
		move.b #$00,splashflag
		rte		
		
	include "hex2bcdV2.asm"		
	include "editor.asm"
	include "titleloop.asm"
	include "clock.asm"
	include "stages.asm"
	include "music_driver_V2.asm"
	include "crashscreen.asm"
	include "decompress.asm"
	include "data.asm"

ROM_End:

 dc.b "The grabbing hands grab all they can, all for themselves after all."
              
              