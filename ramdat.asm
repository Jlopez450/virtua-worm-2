vgm_start = $FF0000 ;long
direction = $FF0004
counter1 = $FF0006
playerx =  $FF0008
playery =  $FF000A
stage = $FF000C
titleflag = $FF000e
dots = $FF0010
totaldots = $FF0012
vb_flag = $FF0014

;clock stuff
vblanks = $ff0016
seconds = $ff0018
minutes = $ff001a
region = $ff001c
;------------------

turn = $ff001e ;self fold fix
splashflag = $ff0020

randomx = $ff0022
randomy = $ff0024
hblanks = $ff002a
splashflag = $ff002c
frenzyflag = $ff002e
frenzycounter = $ff0030
inverter1 = $ff0032
f_frame = $ff0034
inverter2 = $ff0036
lives = $ff0038
editorflag = $ff003a
helpflag = $ff003c

decompression_buffer = $FF2000

;character shit
level_buffer = $FF00F0
segmentqueue = $FF1000
decloc = $FF7000

;editor shit
cursordata = $ff0800  ;for menu
cursordata2 = $ff0808 ;for map
selection = $ff0810; selected block
mode = $ff0812 ;00 for placement, ff for selection
tile = $ff0814 ;selected tile in bytes
sramflag = $ff0816
musicnum = $ff0818;music ID
rules = $ff081a ;00 for normal rules, ff for original rules
levelfood = $ff081c ;food in level
userstage = $ff081e ;for getting to the editor after dying in a user stage
bcdout = $ff0820 ;8 byte
palcounter = $ff0824
speed_frenzy = $ff0826
speed = $ff0828

;crash stuff
;----------------------------
checksum = $FFB000
D0_temp = $FFAF00
D1_temp = $FFAF04
D2_temp = $FFAF08
D3_temp = $FFAF0c
D4_temp = $FFAF10
D5_temp = $FFAF14
D6_temp = $FFAF18
D7_temp = $FFAF1c
A0_temp = $FFAF20
A1_temp = $FFAF24
A2_temp = $FFAF28
A3_temp = $FFAF2c
A4_temp = $FFAF30
A5_temp = $FFAF34
A6_temp = $FFAF38
A7_temp = $FFAF3c
sr_temp = $FFAF40
pc_temp = $FFAF44
errorID = $FFAF50
