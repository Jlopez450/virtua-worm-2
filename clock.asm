clock:
        clr d7
		clr d4
		move.b #$01, d4         ;BCD add by d4 (#1)
		move.b vblanks, d7
		abcd d4,d7              ;add binary coded decimal
        move.b d7, vblanks		;store result
	    cmp.b region, d7        ;see if we should a second
		 beq add_second
        rts	
add_second:
		cmpi.b #$ff, frenzyflag
		beq frenzy
nofrenzy:
        move.b #$00, vblanks	
		move.b #$01, d4	
		move.b seconds, d7	
		abcd d4, d7
		move.b d7, seconds 	
		cmpi.b #$60, seconds
		 beq add_minute
		rts
add_minute:
        move.b #$00, seconds		
		move.b #$01, d4
		move.b minutes, d7	
		abcd d4, d7
		move.b d7, minutes
		cmpi.b #$60, minutes
		 bge reset_clock
		rts
reset_clock:
        move.b #$00,vblanks	
        move.b #$00,seconds		
        move.b #$00,minutes	
		rts					
region_check:
		clr d0
        move.b  $A10001, d0
		andi.b #$40, d0
		cmpi.b #$40, d0
		 beq pal 
		bra ntsc
pal:
		move.b #$50, region
		move.b #$04,speed_frenzy
		move.b #$06,speed
		rts
ntsc:				
        move.b #$60, region
		move.b #$05,speed_frenzy
		move.b #$08,speed
		rts		
		
frenzy:
		bsr add_food
		
		move.b frenzycounter,d0
		move.b #$01,d1
		sbcd d1,d0
		move.b d0,frenzycounter
		;sub.b #$01, frenzycounter
		cmpi.b #$00,frenzycounter
		bne nofrenzy
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop
		move.b #$00,frenzyflag
		bra nofrenzy