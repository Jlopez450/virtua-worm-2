;Note: Must shut of vint and re-enable for all except first stage
;Note2 nevermind
newstage:	
 		move.b #$9F,$c00011
		move.b #$EF,$c00011	;kill psg
        move.b #$FF,$c00011		
        move.b #$BF,$c00011
		move.b #$df,$C00011 ;psg off
		move.b #$00,frenzycounter
		move.b #$00,frenzyflag
		tst.b userstage
		bne load_editor_sram
		lea (stages),a0
		moveq #$00000000,d0
		move.b stage,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)

stages: 
 dc.w ld_stage8 ;tutorial, done
 dc.w ld_stage3 ;Arrow, MEDIUM
 dc.w ld_stage6 ;JL, easy
 dc.w ld_stage1 ;spaceship, EASY
 dc.w ld_stage2 ;tubes, medium  ;MUSIC CHANGE
 dc.w ld_stage9 ;dinner, MEDIUM
 dc.w ld_stage4 ;twist face 
 dc.w ld_stage5 ;med-hard  		;MUSIC CHANGE
 dc.w ld_stage10;boxes
 dc.w start
 dc.w start
 dc.w start		;null stages to correct for BCDS
 dc.w start
 dc.w start
 dc.w start
 dc.w ld_stage7 ;hard, final	
 dc.w ending
 dc.w start

ld_stage1:
		move.w #$2700,sr
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$04,direction
		move.w #$0100,totaldots
		move.w #$0000,dots
		move.w #$0010,playerX; in tiles
		move.w #$0008,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level1),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music1)+66,a6
		move.l a6,VGM_start		
		move.w #$FF90, d7	
		move.w #$8A00,(a3)
		move.b #$ff,splashflag	
		bsr update_hud		
		move.w #$2300,sr
		rts
		
ld_stage2:
		move.w #$2700,sr
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$04,direction
		move.w #$0150,totaldots
		move.w #$0000,dots
		move.w #$0002,playerX; in tiles
		move.w #$0019,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level2),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music2)+66,a6
		move.l a6,VGM_start
		move.w #$FF90, d7	
		move.w #$8A00,(a3)
		move.b #$ff,splashflag	
		bsr update_hud		
		move.w #$2300,sr
		rts
		
ld_stage3:
		move.w #$2700,sr
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0100,totaldots
		move.w #$0000,dots
		move.w #$0002,playerX; in tiles
		move.w #$000C,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level3),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music1)+66,a6
		move.l a6,VGM_start
		move.w #$FF90, d7	
		move.w #$8A00,(a3)
		move.b #$ff,splashflag	
		bsr update_hud		
		move.w #$2300,sr
		rts
		
ld_stage4:
		move.w #$2700,sr
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0100,totaldots
		move.w #$0000,dots
		move.w #$0002,playerX; in tiles
		move.w #$000C,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level4),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music2)+66,a6
		move.l a6,VGM_start
		move.w #$FF90, d7	
		move.w #$8A00,(a3)
		move.b #$ff,splashflag	
		bsr update_hud		
		move.w #$2300,sr
		rts
		
ld_stage5:
		move.w #$2700,sr
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0066,totaldots
		move.w #$0000,dots
		move.w #$0002,playerX; in tiles
		move.w #$000f,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level5),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (ogmusic3)+66,a6
		move.l a6,VGM_start
		move.w #$FF90, d7	
		move.w #$8A00,(a3)
		move.b #$ff,splashflag	
		bsr update_hud		
		move.w #$2300,sr
		rts
		
ld_stage6:
		move.w #$2700,sr
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0125,totaldots
		move.w #$0000,dots
		move.w #$000e,playerX; in tiles
		move.w #$0019,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level6),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music1)+66,a6
		move.l a6,VGM_start
		move.w #$FF90, d7	
		move.w #$8A00,(a3)
		move.b #$ff,splashflag	
		bsr update_hud		
		move.w #$2300,sr
		rts
		
ld_stage7:
		move.w #$2700,sr
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0175,totaldots
		move.w #$0000,dots
		move.w #$0013,playerX; in tiles
		move.w #$0003,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level7),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (ogmusic3)+66,a6
		move.l a6,VGM_start
		move.w #$FF90, d7	
		move.w #$8A00,(a3)
		move.b #$ff,splashflag	
		bsr update_hud		
		move.w #$2300,sr
		rts
		
ld_stage8:
		move.w #$2700,sr
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		move.w #$0025,totaldots
		move.w #$0000,dots
		move.w #$0008,playerX; in tiles
		move.w #$0008,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level8),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music1)+66,a6
		move.l a6,VGM_start
		move.w #$FF90, d7	
		move.w #$8A00,(a3)
		move.b #$ff,splashflag	
		bsr update_hud		
		move.w #$2300,sr
		rts
		
ld_stage9:
		move.w #$2700,sr
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$04,direction
		move.w #$0125,totaldots
		move.w #$0000,dots
		move.w #$0003,playerX; in tiles
		move.w #$0004,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level9),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (music2)+66,a6
		move.l a6,VGM_start
		move.w #$FF90, d7	
		move.w #$8A00,(a3)
		move.b #$ff,splashflag	
		bsr update_hud		
		move.w #$2300,sr
		rts
		
ld_stage10:
		move.w #$2700,sr
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$01,direction
		move.w #$0125,totaldots
		move.w #$0000,dots
		move.w #$0019,playerX; in tiles
		move.w #$0019,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (level10),a5
		move.l #$60000002,(a3)
		bsr build_stage
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud	
		lea (ogmusic3)+66,a6
		move.l a6,VGM_start
		move.w #$FF90, d7	
		move.w #$8A00,(a3)
		move.b #$ff,splashflag	
		bsr update_hud		
		move.w #$2300,sr
		rts
		
ld_teststage:
		tst.b sramflag
		bne ld_sramstage
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$02,direction
		;move.w #$0099,totaldots
		move.w #$0000,dots
		move.w #$0002,playerX; in tiles
		move.w #$000C,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (teststage),a5
		move.l #$60000002,(a3) ;replace this for loading sram stage
		bsr build_stage
		move.b #$01,musicnum
		lea (ogmusic1)+66,a6
		move.l a6,VGM_start
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud
		move.b #$00,splashflag	
		bsr update_hud		
		rts
		
ld_sramstage:
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		; move.b #$02,direction
		;move.w #$0115,totaldots
		move.w #$0000,dots
		; move.w #$0002,playerX; in tiles
		; move.w #$000C,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2	
		bsr load_to_editor
		bsr load_music
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud
		move.b #$00,splashflag	
		bsr update_hud	
		rts

ld_userstage:
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		; move.b #$02,direction
		;move.w #$0115,totaldots
		move.w #$0000,dots
		; move.w #$0002,playerX; in tiles
		; move.w #$000C,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2	
		bsr load_music		
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud
		move.b #$00,splashflag
		bsr update_hud		
		rts

ending:	
		move.w #$2700,sr
		lea (palette1),a5
		move.l #$C0000000,(a3)
		move.w #$2f,d4
		bsr vram_loop	
		move.b #$04,direction
		move.w #$0FFF,totaldots
		move.w #$0000,dots
		move.w #$0002,playerX; in tiles
		move.w #$0004,playerY; in tiles
		lea (segmentqueue),a1
		lea (segmentqueue),a2		
		lea (endingstage),a5
		move.l #$60000002,(a3)
		bsr build_stage		
		lea (hud),a5
		move.l #$60360002,(a3)
		bsr build_hud
		lea (endtext),a5
		move.l #$0000a104,d5
		bsr draw_dialogue
		lea (ogmusic2)+66,a6
		move.l a6,VGM_start		
		move.w #$2300,sr
		rts	