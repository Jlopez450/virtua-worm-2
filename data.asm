VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ;Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8228	;field A    
	dc.w $8300	;$833e	
	dc.w $8406	;field B	
	dc.w $857c	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8Aff		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D3C		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
palette1:;Violet BG green food blue player
	dc.w $0000,$0888,$0606,$0E0E,$0262,$02a2,$02E2,$0E00
	dc.w $0000,$0E44,$00E0,$0E66,$0E88,$0ECC,$0000,$00E0 ;fg	
	dc.w $0000,$0eee,$0000,$0000,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0EEE ;txt
	dc.w $0000,$0202,$0404,$0000,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000 ;bg

palette2:;frenzy mode
	dc.w $0000,$0CCC,$0060,$00E0,$0622,$0a22,$0e22,$000E
	dc.w $0000,$044E,$0E00,$044E,$066E,$088C,$0000,$00E0 ;fg	
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0EEE ;txt
	dc.w $0000,$0020,$0040,$0000,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000 ;bg
	
titlepalette:
	dc.w $0000,$0000,$0600,$0E00,$0C00,$0A00,$0200,$000C
	dc.w $000A,$000E,$0004,$0040,$0060,$0080,$00A0,$0020
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0eee
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0eee
	
palette_splash:
	dc.w $0000,$0000,$0662,$0222,$0200,$0686,$0666,$0EC8
	dc.w $0886,$0888,$0C86,$0C82,$0EEE,$06CC,$0CC6,$08CE ;mode 5
	dc.w $0000,$0000,$0000,$0eee,$026A,$000E,$0AAA,$02A2 ;text
	dc.w $006E,$0CAE,$048C,$0000,$086C,$00EE,$0000,$0eee
	dc.w $0000,$0000,$0000,$0eee,$026A,$000E,$0AAA,$02A2 ;text
	dc.w $006E,$0CAE,$048C,$0000,$086C,$00EE,$0000,$0eee	
	
f_frame0:
	dc.l	$00655600	; Tile #0
	dc.l	$06544560
	dc.l	$66544566
	dc.l	$66544566
	dc.l	$66544566
	dc.l	$66544566
	dc.l	$06544560
	dc.l	$00655600
f_frame1:
	dc.l	$00666500	; Tile #0
	dc.l	$06665440
	dc.l	$66654445
	dc.l	$66544456
	dc.l	$65444566
	dc.l	$54445666
	dc.l	$04456660
	dc.l	$00566600
f_frame2:
	dc.l	$00666600	; Tile #0
	dc.l	$06666660
	dc.l	$65555556
	dc.l	$54444445
	dc.l	$54444445
	dc.l	$65555556
	dc.l	$06666660
	dc.l	$00666600
f_frame3:
	dc.l	$00566600	; Tile #0
	dc.l	$04456660
	dc.l	$54445666
	dc.l	$65444566
	dc.l	$66544456
	dc.l	$66654445
	dc.l	$06665440
	dc.l	$00666500
	
cursor:
 incbin "gfx/cursor.bin"


	
titletext:
 dc.b "  Press START to play$"
 dc.b " $"
 dc.b "  Press A to create a stage$"
 dc.b "  Press B to load a stage$"
 dc.b "  Press C for help $"  
 dc.b " %"
 
helptext:
 dc.b " How to play:                            $"
 dc.b "  Control yourself with the d-pad.       $"
 dc.b "  Eat the specified amount of dots.      $"
 dc.b "  Don't run into your own tail!          $"
 dc.b "                                         $"
 dc.b " Power-ups:                              $"
 dc.b "  Frenzy sphere: Spawns 1 food/second    $"
 dc.b "  for 5 seconds while active.            $"
 dc.b "  Head icon: Grants 1 additional life.   $"
 dc.b "                                         $"
 dc.b " Tips:                                   $"
 dc.b "  Plan your route carefully              $"
 dc.b "  Grab 1-ups before you get too big      $"
 dc.b "                                         $"
 dc.b " Editor help:                            $"
 dc.b "  Editor controls:                       $"
 dc.b "  START button=Switch modes              $" 
 dc.b "  D-Pad: Move cursor (both modes)        $" 
 dc.b "  A=Fast cursor (while held; both modes) $"
 dc.b "   Insert mode:                          $"
 dc.b "    B=Place selected block               $"
 dc.b "    C=Remove block                       $"
 dc.b "   Select mode:                          $"
 dc.b "    B=Save stage and continue            $"
 dc.b "    C=Save stage and play                $"
 dc.b "                                         $"
 dc.b "           Contact: ComradeOj@yahoo.com  $"
 dc.b "                                         %" 
 
endtext: ;25 lines for end stage
 dc.b "Thank you for playing!$"
 dc.b "I hope you enjoyed... $"
 dc.b "$" 
 dc.b "     Virtua Worm 2!$"
 dc.b "$" 
 dc.b " Don't forget to check$" 
 dc.b " out the level creator!$" 
 dc.b " Also play the original$"
 dc.b "   Virtua Worm game.$" 
 dc.b "$" 
 dc.b "Some fun facts:$" 
 dc.b "$" 
 dc.b "ROM Build date: $" 
 dc.b "October 09, 2018$" 
 dc.b "$" 
 dc.b "ROM Size in bytes:$" 
 dc.b "206,708$ " 
 dc.b "$"
 dc.b "Cool sites to visit:$" 
 dc.b "$" 
 dc.b "www.sega-16.com$" 
 dc.b "www.tmeeco.eu$" 
 dc.b "www.mode5.net$" 
 dc.b "gendev.spritesmind.net$" 
 dc.b "reddit.com/r/retrogaming%" 
 
killtext:
 dc.b "          Chomp!         $"
 dc.b "Press START to try again!%"
 
titlescroll:
 dc.b "Code, music, and graphics by James Lopez. ", $7f, "2018 Visit mode5.net  %"
 ;dc.b "| Super duper mega top secret BETA build! ", $7f, "2018 Visit mode5.net  %"
 
hud:
 dc.b "             "
 dc.b "Virtua Worm2 "
 dc.b "             "
 dc.b "Time-   :    "
 dc.b "             "
 dc.b "Dots-   /    "
 dc.b "             "
 dc.b "Level-       "
 dc.b "             "
 dc.b "Frenzy-      "
 dc.b "             "
 dc.b "Lives-       "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "             "
 dc.b "  mode5.net  "
 dc.b "            %"
 
ed0:
 dc.b " W w a b c F"
ed1:
 dc.b " A B C D d f"
ed2:
 dc.b " X T t I i"
ed3:
 dc.b " 1 2 3 4 5 L"
 
ruletext:
 dc.b "Rule type? $"
 dc.b "           $"
 dc.b "A=original $"
 dc.b "B=V.Worm 2 %"
dirtext:
 dc.b "Set your   $"
 dc.b "starting   $"
 dc.b "position!  $"
 dc.b "C=begin    %"
clearruletext:
 dc.b "           $"
 dc.b "           $"
 dc.b "           $"
 dc.b "           %"
 
;Playfield = 26x26 + 1 tile for CRT crop
;W = vert wall
;w = horiz wall
;X = + junction
;Tt = vertical T intersection
;Ii = horizontal T intersection
;ABCD = bends starting top left clockwise
;abcd = left, right, up, down caps respectivley
;.=deadzone (for inaccessable areas)
	
level1:
 dc.b "                                        "	;crop
 dc.b " AwwwwwTwwTwwwwwwTwwTwwwwwB             "
 dc.b " W.....WFfW......WLFW.....W             "
 dc.b " W.....W FDwB..AwCF W.....W             "
 dc.b " W.....W    DwwC    W.....W             "
 dc.b " W.....W            W.....W             "
 dc.b " W.....W            W.....W             "
 dc.b " iwwwwwC    AwwB    DwwwwwI             "
 dc.b " WF         W..W         FW             "
 dc.b " W   AwB    W..W    AwB   W             "
 dc.b " W   W.W  AwC..DwB  W.W   W             "
 dc.b " W   W.W  W......W  W.W   W             "
 dc.b " W AwC.DwwC......DwwC.DwB W             "
 dc.b " W W....................W W             "
 dc.b " W W....................W W             "
 dc.b " W DwB.AwwB......AwwB.AwC W             "
 dc.b " W   DwC  W......W  DwC   W             "
 dc.b " W        DwwwwwwC        W             "
 dc.b " W awwwb     FF     awwwb W             "
 dc.b " WF          FF          FW             "
 dc.b " iwwwwwBFc c AB c cFAwwwwwI             "
 dc.b " W.....W WFW WW WFW W.....W             "
 dc.b " W.....WFW WFWWFW WFW.....W             "
 dc.b " W.....W WFW WW WFW W.....W             "
 dc.b " W.....W d dFDCFd d W.....W             "
 dc.b " W.....W F F FF F F W.....W             "
 dc.b " DwwwwwtwwwwwwwwwwwwtwwwwwC             "
 dc.b "                                       %"
 
level2:
 dc.b "                                        "
 dc.b " AwwwwwwwwwwwwwwTwwwTwwwwwB             "
 dc.b " W   F          W f W     W             "
 dc.b " W cFawwwwwwwTB WFcFW c   W             "
 dc.b " W W F       WW W W W W   W             "
 dc.b " W iwwwwwwwbfWW W W W W   W             "
 dc.b " W W         WW W W W W   W             "
 dc.b " W W awwwwwwwIW W W W W   W             "
 dc.b " W W       F WW W W W W   W             "
 dc.b " W DwwwwwwwbFWWFdFWFdFW   W             "
 dc.b " W         F WW F W F W   W             "
 dc.b " W AwwwwwwwwwXXwwwtwwwtwB W             "
 dc.b " W DwwwwwwwwwXXwwwwwwwwwC W             "
 dc.b " W F   F   F DCF   F   F  W             " 
 dc.b " W   F   F   F   F   F    W             " 
 dc.b " W AwwwwwwwwwwwwwwwwwwwwB W             "
 dc.b " W DTwwB.AwwwwwwwwB.AwwTC W             "
 dc.b " W  WFFW.W F F F FW.WFFW  W             "
 dc.b " W  WFFW.WF F F F W.WFFW  W             "
 dc.b " W  W  W.W FAwwB FW.W  W  W             "
 dc.b " W  W  W.WF W..WF W.W  W  W             "
 dc.b " W  W  W.W FW..W FW.W  W  W             "
 dc.b " W  W  W.WF W..WF W.W  W  W             "
 dc.b " W  W  W.W FW..W FW.W  W  W             "
 dc.b " W  d  DwCF DwwCF DwC  d  W             "
 dc.b " W                        W             "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC             "
 dc.b "                                       %"
 
level3:
 dc.b "                                        "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB             "
 dc.b " WF                      FW             "
 dc.b " W AB AwwwwwB  AwwwwwB AB W             "
 dc.b " W DC DwwwwwC  DwwwwwC DC W             "
 dc.b " W                        W             "
 dc.b " W AB awwwwwb AB aB    AB W             "
 dc.b " W WW F F F F WW  DB   WW W             "
 dc.b " W WW awwwwwb WWFFFDB  WW W             "
 dc.b " W WW  F F F  WWFcfFDB WW W             "
 dc.b " W WW awwwwwb WWFDbF d WW W             "
 dc.b " W WW F F F F WWFFFF   WW W             "
 dc.b " W DC awwwwwb WDwwwwwB DC W             "
 dc.b " W     F F F  DwwwwwwC    W             "
 dc.b " W    AwwwwwwB            W             "
 dc.b " W AB DwwwwwBWFc cFc cFAB W             "
 dc.b " W WW F F FfWW WFW WFW WW W             "
 dc.b " W WW AwwwBFWWFW WFW WFWW W             "
 dc.b " W WW W...W WW WFW WFW WW W             "
 dc.b " W WW W...WFWWFW WFW WFWW W             "
 dc.b " W WW W...W WW WFW WFW WW W             "
 dc.b " W DC DwwwCFDCFd dFd dFDC W             "
 dc.b " W                        W             "
 dc.b " W AB AwwwwwB  AwwwwwB AB W             "
 dc.b " W DC DwwwwwC  DwwwwwC DC W             "
 dc.b " WF                      FW             "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC             "
 dc.b "                                       %"
 
level4:
 dc.b "                                        "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB             "
 dc.b " WF                      FW             "
 dc.b " W AB ATwwTwwwwwwwTwwwwwB W             "
 dc.b " W DC DC  d       d    FW W             "
 dc.b " W   f  F     c       c W W             "
 dc.b " W AB AB ATwwwtwwwwwwwC W W             "
 dc.b " W iC DC DCF F F F F F  W W             "
 dc.b " W W F  F F   AB   Ab c W W             "
 dc.b " W W  AB AwB  DC  AwCFW W W             "
 dc.b " W ib iCFW.DB    AwC  W W W             "
 dc.b " W W  WF W..DB  AwC  FW W W             "
 dc.b " W W  W FW...DB aC    W W W             "
 dc.b " W W  WF iwwB.DB   ABFW W W             "
 dc.b " W W aI FW  DB.DB  DC W W W             "
 dc.b " W W  WF W   DB.DB   FW W W             "
 dc.b " W W  W FW c LDB.DB   W W W             "
 dc.b " W W  WF W iB  DB.DB FW W W             "
 dc.b " W ib W Fd Dtb  W..DB W W W             "
 dc.b " W W  WF        W...WFW W W             "
 dc.b " W W  W F  awwwwtwwwC W W W             "
 dc.b " W W  W  F F F F F F FW W W             "
 dc.b " W W aC awwwwwwwwwwwwwC d W             "
 dc.b " W WF                  F  W             "
 dc.b " W Dwwwwwwwwwwwwwwwwwwb   W             "
 dc.b " WF                      FW             "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC             "
 dc.b "                                       %"
 
level5 ;robot head
 dc.b "                                        "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB             "
 dc.b " W                        W             "
 dc.b " W awwwwwwB c  c Awwwwwwb W             "
 dc.b " WF      fW W  W Wf      FW             "
 dc.b " W   c    W W  W W  c  c  W             "
 dc.b " W  AIAB  W W  W W aC  Db W             "
 dc.b " W atXXC  W W  W W   AB   W             "
 dc.b " W  AXXTb W W  W W   DC   W             "
 dc.b " W  DCiC  W W  W W aB  Ab W             "
 dc.b " W    d   W W  W W  d  d  W             "
 dc.b " WF        FW  WF        FW             "
 dc.b " iwwwwwTwwwwC  DwwwwTwwwwwI             "
 dc.b " WF    d            d    FW             "
 dc.b " W                        W             "
 dc.b " W c  c AwwwB  AwwwB c  c W             "
 dc.b " W W  W W F W  W F W W  W W             "
 dc.b " W W  W WFWFW  WFWFW W  W W             "
 dc.b " W W  W W W ib W W W W  W W             "
 dc.b " W W  WFWFWFWFFWFWFWFW  W W             "
 dc.b " W W  WFW W WFFW W WFW  W W             "
 dc.b " W W  W W W W wI W W W  W W             "
 dc.b " W W  W W W W  W W W W  W W             "
 dc.b " W WFFW W W W  W W W WFFW W             "
 dc.b " W DwwC d d d  d d d DwwC W             "
 dc.b " WF                      FW             "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC             "
 dc.b "                                       %"
 
level6:;nibblet
 dc.b "                                        "
 dc.b " AwwwwTwwwwwwwwwwwwwwTwwwwB             "
 dc.b " WF F d F F F F F F FdF F W             "
 dc.b " W FcF FcF F F F F c F c FW             "
 dc.b " W  DwwwtwwwwwwwwwwtwwwCF W             "
 dc.b " WF                      FW             "
 dc.b " W Fc awwwwwwwwwwwwwwB cF W             "
 dc.b " WF W                W W FW             "
 dc.b " W FiwwwwwwwwwwwwwwbLW WF W             "
 dc.b " WF W                W W FW             "
 dc.b " W FW awwwwwwTTwwwwwwC WF W             "
 dc.b " WF W        WW        W FW             "
 dc.b " W FW c      WW      c WF W             "
 dc.b " WF W W    F WW F    W W FW             "
 dc.b " W FW W    F WW F    W WF W             "
 dc.b " WF W W    F WW F    W W FW             "
 dc.b " W FW W    F WW F    W WF W             "
 dc.b " WF W W F  F WW F    W W FW             "
 dc.b " W FW W F  F WW F    W WF W             "
 dc.b " WF W W  FF  WW FFFF W W FW             "
 dc.b " W FW W      WW      W WF W             "
 dc.b " WF W W      DC      W W FW             "
 dc.b " W FW Wf            fW WF W             "
 dc.b " WF d DwwwwwTwwTwwwwwC d FW             "
 dc.b " W F        DwwC        F W             "
 dc.b " WF   c              c   FW             "
 dc.b " DwwwwtwwwwwwwwwwwwwwtwwwwC             "
 dc.b "                                       %"
 
level7: ;Final!
 dc.b "                                        "
 dc.b " AwwwwwwwwwwwwwwwwwwTwwwwwB             "
 dc.b " Wf                 WF   FW             "
 dc.b " W                c W AwB W             "
 dc.b " W                W W W.W W             "
 dc.b " W                W W W.W W             "
 dc.b " W                W WFW.WFW             "
 dc.b " W Awwwwwwwwwwwwb W WFW.WFW             "
 dc.b " W WFFFFFFFFFFFF  W W W.W W             "
 dc.b " W WF             W W W.W W             "
 dc.b " W WF AwwwwwwwwwB W d W.W W             "
 dc.b " W WF WFFFFFFFFFW W  FW.WFW             "
 dc.b " W WF WF       FW W AwtwC DwwwwwwwwwwwB "
 dc.b " W WF WF awwwB FW W WFFFF            fW "
 dc.b " W WF WF   LFW FW W W  FFFAwwwwwwwwwB W "
 dc.b " W WF WFFFFFFW FW W W   FFW         W W "
 dc.b " W WF DwwwwwwC FW W W   FFW         W W "
 dc.b " W WF          FW W W  FFFW         W W "
 dc.b " W WFFFFFFFFFFFFW W WFFFF W         WLW "
 dc.b " W DwwwwwwwwwwwwC W WFF   W         W W "
 dc.b " W                W WFF   W         W W "
 dc.b " W  AwwwwwwwwwwB  W WFF   W         W W "
 dc.b " W  WL L L L LfW  W W     W         W W "
 dc.b " W  W L L L L LW  W WFF   DwwwwwwwwwC W "
 dc.b " W  WL L L L L W  d dFF              fW "
 dc.b " W  WfL L L L LW          A CwwwwwwwwwC "
 dc.b " DwwtwwwwwwwwB DwwwwwwwwwwC W           "
 dc.b "             W              W          %"
 
level8: ;tutorial
 dc.b "                                        "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB             "
 dc.b " W........................W             "
 dc.b " W........................W             "
 dc.b " W........................W             "
 dc.b " W........................W             "
 dc.b " W....AwwwwwwwwwwwwwwB....W             "
 dc.b " W....Wf            FW....W             "
 dc.b " W....W              W....W             "
 dc.b " W....W              W....W             "
 dc.b " W....W              W....W             "
 dc.b " W....W              W....W             "
 dc.b " W....W     FFFF     W....W             "
 dc.b " W....W     F  F     W....W             "
 dc.b " W....W     F  F     W....W             "
 dc.b " W....W     FFFF     W....W             "
 dc.b " W....W              W....W             "
 dc.b " W....W              W....W             "
 dc.b " W....W              W....W             "
 dc.b " W....W              W....W             "
 dc.b " W....WF            LW....W             "
 dc.b " W....DwwwwwwwwwwwwwwC....W             "
 dc.b " W........................W             "
 dc.b " W........................W             "
 dc.b " W........................W             "
 dc.b " W........................W             "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC             "
 dc.b "                                       %"
 
level9:
 dc.b "                                        "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB             "
 dc.b " WfF                    FfW             "
 dc.b " WFawwwwwwwwwwwB         FW             "
 dc.b " W   F F F F FFDwwwwwwwwb W             "
 dc.b " W awwwwwwwwBFF F F F F   W             "
 dc.b " W          DTTwwwwwwwwwb W             "
 dc.b " W           WW           W             "
 dc.b " W c         WW         c W             "
 dc.b " W W       AwttwB     c W W             "
 dc.b " WFW       DwwwwC     W WFW             "
 dc.b " W W   c              W W W             "
 dc.b " WFW   W              W WFW             "
 dc.b " W W   W     FF   awTwC W W             "
 dc.b " WFW Awtwb  FFFF    W   WFW             "
 dc.b " W W W     AwwwwB   W   W W             "
 dc.b " WFW W    AC....DB  d   WFW             "
 dc.b " W W W   AC......DB     W W             "
 dc.b " WFW d  AC........DB    WFW             "
 dc.b " W W   AC..........DB   W W             "
 dc.b " W d  AC............DB  d W             "
 dc.b " W   AC..............DB   W             "
 dc.b " W  AC................DB  W             "
 dc.b " W AC..................DB W             "
 dc.b " WFDwwwwwwwwwwwwwwwwwwwwCFW             "
 dc.b " WfF                    FfW             "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC             "
 dc.b "                                       %"
 
level10:
 dc.b "                                        "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB             "
 dc.b " W                        W             "
 dc.b " W AB AB AB AwwB AB AB AB W             "
 dc.b " W DC DC DC DTTC WW WW DC W             "
 dc.b " W    F F F FWWF WW WW    W             "
 dc.b " W ABF       WW  WW WW AB W             "
 dc.b " W DC  AwwB FWWF WWfWW DC W             "
 dc.b " W   F W..W  WW  WW WW    W             "
 dc.b " W AB  W..W FWWF WW WW AB W             "
 dc.b " W DCF DwwC  WW  WW WW DC W             "
 dc.b " W          FWWF DC DC    W             "
 dc.b " W ABF F F F WW F F F FAB W             "
 dc.b " W WiwwwwwwwwttwwwwwwwwIW W             "
 dc.b " W WiwwwwwwwwwwwwwwwwwwIW W             "
 dc.b " W DCF F F F F F F F F DC W             "
 dc.b " W    F F F F F F F F F   W             "
 dc.b " W AwwwwwwwwwwwwwwwwwB AB W             "
 dc.b " W DwwwwwwwwwwwwwwwwwCFDC W             "
 dc.b " W   F F F F F F F F F F  W             "
 dc.b " W AB AwwwwwwwwwwwwwwwwwB W             "
 dc.b " W DC DwwwwwwwwwwwwwwwwwC W             "
 dc.b " W                        W             "
 dc.b " W AB AB AB AwwB AB AB AB W             "
 dc.b " W DC DC DC DwwC DC DC DC W             "
 dc.b " W                        W             "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC             "
 dc.b "                                       %"

teststage:;currently used for level editor
 dc.b "                                        "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " W                        W             "
 dc.b " DwwwwwwwwwwwwwwwwwwwwwwwwC             "
 dc.b "                                       %"
 
endingstage:
 dc.b "                                        "
 dc.b " AwwwwwwwwwwwwwwwwwwwwwwwwB             "
 dc.b " WFFFFF FFF FFF FFFFFFFF  W             "
 dc.b " WF FFFF FFF FFFFFFFFFF   W             "
 dc.b " W                        W             "
 dc.b " W     FFFFFF FFFF FF     W             "
 dc.b " W                        W             "
 dc.b " W FFFFF FFFFFF FF FFFFF  W             "
 dc.b " W FFF FFF FFFFF FFFFFFFF W             "
 dc.b " W FFFF FFFF FFF FFFFFFFF W             "
 dc.b " W   FFFFFFFFFFFFFFFFF    W             "
 dc.b " W                        W             "
 dc.b " WFFFF FFF FFFFFF         W             "
 dc.b " W                        W             "
 dc.b " WFFF FFFFF FFFFF         W             "
 dc.b " WFFFFFFFF FFF FFF        W             "
 dc.b " W                        W             "
 dc.b " WFFF FFFF FF FFFFFF      W             "
 dc.b " WFFFFFFF                 W             "
 dc.b " W                        W             "
 dc.b " WFFFF FFFFF FF FFFFFF    W             "
 dc.b " W                        W             "
 dc.b " WFFFFFFFFFFFFFFF         W             "
 dc.b " WFFFFFFFFFFFFF           W             "
 dc.b " WFFFFFFFFFFFFF           W             "
 dc.b " WFFFFFFFFFFFFFFFFFFFFFF  d             "
 dc.b " WFFFFFFFFFFFFFFFFFFFFFFFF              "
 dc.b " Dwwwwwwwwwwwwwwwwwwwwwwwwb            %" 

splashtext:
 dc.b "Visit www.mode5.net$"
 
sfx_slogan:
 incbin "music\slogan.pcm"
 
font:
 incbin "gfx/ascii.bin"
 incbin "gfx/hexascii.bin"
 incbin "gfx/tiles.bin"
 
titlescreen:
 incbin "gfx/titlescreen.kos"
 
background:
 incbin "gfx/background2.kos"
 
splashscreen:
 incbin "gfx\splash.kos" 
 
;thunkfx:
 ;incbin "music/thunk.fm"

music_null:
 incbin "music/mute.vgm" 
 
titletheme:
 incbin "music/RB12.vgm" 
music1:
 incbin "music/RB13.vgm" 
music2:
 incbin "music/RB15.vgm" 
music3:
 incbin "music/RB7.vgm"  
ogmusic1:
 incbin "music/music1.vgm"
ogmusic2:
 incbin "music/music2.vgm"
ogmusic3:
 ;incbin "music/music3.vgm" ;replaced with new
 incbin "music/RB1_1.vgm"
 